//
//  FirstViewController.swift
//  ToDoList
//
//  Created by Gerson Costa on 05/06/2018.
//  Copyright © 2018 Gerson Costa. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var toDoList: [String] = []
    
    
    //MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        loadList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        
        loadList()
        tableView.reloadData()
    }
    
    //MARK: Table Protocols
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toDoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.textLabel?.text = toDoList[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            toDoList.remove(at: indexPath.row)
            UserDefaults.standard.set(toDoList, forKey: "List")
            tableView.reloadData()
        }
    }
    
    //MARK: Helpers
    func loadList() {
        if let objectArray = UserDefaults.standard.object(forKey: "List") {
            toDoList = objectArray as! [String]
        }
    }
    
    
    
}

