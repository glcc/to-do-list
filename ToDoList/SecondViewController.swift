//
//  SecondViewController.swift
//  ToDoList
//
//  Created by Gerson Costa on 05/06/2018.
//  Copyright © 2018 Gerson Costa. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var itemTxtField: UITextField!
    @IBOutlet weak var feedbackLbl: UILabel!
    
    var toDoList: [String] = []
    var seconds = 0
    var timer = Timer()
    
    //MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        itemTxtField.delegate = self
        
        if let objectArray = UserDefaults.standard.object(forKey: "List") {
            toDoList = objectArray as! [String]
        }
    }
    
    //MARK: Keyboard Control
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: Button Press
    @IBAction func addBtnPressed(_ sender: Any) {
        view.endEditing(true)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(showFeedback), userInfo: nil, repeats: true)
        
        if itemTxtField.text != "" {
            let item = itemTxtField.text
            toDoList.append(item!)
            UserDefaults.standard.set(toDoList, forKey: "List")
            feedbackLbl.text = "\(item!) was successfully added to the list."
            itemTxtField.text = ""
            feedbackLbl.isHidden = false
        } else {
            feedbackLbl.text = "Please add an item before saving!"
            feedbackLbl.isHidden = false
        }
    }
    
    //MARK: Helper
    @objc func showFeedback() {
        seconds += 1
        if seconds == 3 {
            timer.invalidate()
            feedbackLbl.isHidden = true
            feedbackLbl.text = ""
            seconds = 0
        }
    }
}

